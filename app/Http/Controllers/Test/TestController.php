<?php
/**
 * Created by PhpStorm.
 * User: DucHai
 * Date: 30/03/2017
 * Time: 8:49 SA
 */
namespace App\Http\Controllers\Test;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class TestController extends BaseController
{
    public function getTestPage(){
        return view("test.index");
    }
}

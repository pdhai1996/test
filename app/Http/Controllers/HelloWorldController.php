<?php
/**
 * Created by PhpStorm.
 * User: DucHai
 * Date: 23/02/2017
 * Time: 4:39 CH
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class HelloWorldController extends Controller
{
   function index(){
       $users=[
           '0'=> ['first_name' => 'Hai','last_name'=>'Pham Duc', 'location'=> 'Viet Nam'],
           '1'=> ['first_name' => 'Bien','last_name'=>'Pham Xuan', 'location'=> 'Chau Quy']
       ]
       ;
       return view('user.index',compact('users'));
   }

   public function getSimpleForm(){
       return view("user.simpleForm");
   }

   public function store(Request $request){
              return $request->all();
   }
}
<?php
/**
 * Created by PhpStorm.
 * User: DucHai
 * Date: 30/03/2017
 * Time: 8:43 SA
 */
?>
<script src="js/jquery.js"></script>
{{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>--}}
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
{{--<link rel="stylesheet" href="{{asset('public/bootstrap/css/bootstrap.min.css')}}" type="text/css">--}}
<link rel="stylesheet" href="css/dotest.css">
<script src="js/dotest.js"></script>
<div class="container">
{{--<div id="test-wrapper" class="container test-item">--}}
    {{--<div class="inner-div container">--}}
        {{--<div class="test-header"></div>--}}
        {{--<div class="test-content">--}}
           {{--<button class="btn-success start-button" id="btn-start">--}}
               {{--<span class="glyphicon glyphicon-send" style="margin-right: 10px"></span><span>Start</span>--}}
           {{--</button>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
<div id="test-wrapper" class="container test-item" >
    <div class="inner-div container">
        <div class="test-header"></div>
        <div class="test-content">
            <div class="question-block container center-block"><p align="center"><span>1/9 |</span><span> Trắc nghiệm chính tả</span></p></div>
            <div class="question-content"><p class="text-center" >Từ nào sau đây dùng để chỉ sự thiếu sót?</p></div>
            <div class="answer-container container">
                <div class="answer" true-false-value="0">
                    <div class="rate-bar" rate-width="30%"></div>
                    <span class="answer-content">Sơ xuất</span>
                    <span hidden class="rate">30%</span>
                </div>
                <div class="answer" true-false-value="1">
                    <div class="rate-bar" rate-width="40%"></div>
                    <span class="answer-content">Sơ suất</span>
                    <span hidden class="rate">40%</span>
                </div>
                <div class="answer" true-false-value="0">
                    <div class="rate-bar" rate-width="30%"></div>
                    <span class="answer-content">Xơ suất</span>
                    <span hidden class="rate">30%</span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<button class="btn btn-danger" id="testJS">Test</button>
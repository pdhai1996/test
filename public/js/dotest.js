/**
 * Created by DucHai on 30/03/2017.
 */
wipe_right($('.rate-bar'));

function wipe_right (elem) {

    $.each(elem,function (idx,itm) {
        var singleElem=$(elem[idx]);
        var width=singleElem.attr("rate-width");
        singleElem.css('width', '0%');
        singleElem.css("right", width);
        singleElem.animate({
            width: width,
            right: '0%'
        }, 1000);
    });
    // elem.css('width', '0%');
    // elem.css("right", width);

}

$(document).ready(function () {

    $("#testJS").click(function () {
        alert("ok");
    });

    var listItems=$(".test-item");
    var idx=0;
    $("#btn-start").click(function () {
        $(listItems[idx]).fadeOut(1000);
        idx++;
        $(listItems[idx]).fadeIn(2000);
    });

    var answers=$(".answer");
    $.each(answers, function (idx,itm) {
       var answer=$(answers[idx]);
       answer.click(function () {
           var parent=answer.parent(".answer-container");
           var rateBars=parent.find(".rate-bar");
           var answers=parent.children(".answer");
           var rates=parent.find(".rate")
           $.each(answers, function (idx,itm) {
              var item=$(answers[idx]);
              var rateBar=item.children(".rate-bar").first();
              if(item.attr("true-false-value")!=1){
                  rateBar.css("background-color","white")
              }
              else
              {
                  rateBar.css("background-color","green")
              }
           });
           if(answer.attr("true-false-value")==0)
           {
            var rateBar=answer.children(".rate-bar").first();
            rateBar.css("background-color","red");
            rateBar.css("opacity","1");
           }
           else {
               var rateBar=answer.children(".rate-bar").first();
               rateBar.css("opacity","1");
           }
           // rateBars.fadeIn(2000);
           wipe_right(rateBars);
           rates.show();
       });
    });
});

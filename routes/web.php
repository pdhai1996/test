<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('dotest',['uses' => 'Test\TestController@getTestPage']);

Route::get('user', ['uses'=>'HelloWorldController@index']);

Route::get('simpleform', ['uses'=>'HelloWorldController@getSimpleForm']);

Route::post('storeinfor', ['uses'=>'HelloWorldController@store']);

//Route::get('user',function (){
//    $user=[
//        '0'=> ['first_name' => 'Hai','last_name'=>'Pham Duc', 'location'=> 'Viet Nam'],
//        '1'=> ['first_name' => 'Bien','last_name'=>'Pham Xuan', 'location'=> 'Chau Quy']
//    ]
//    ;
//    return $user;
//});
